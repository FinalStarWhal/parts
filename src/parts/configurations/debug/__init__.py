from __future__ import absolute_import, division, print_function

from parts.config import DefineConfiguration

DefineConfiguration("debug", dependsOn='default')
