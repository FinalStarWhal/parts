from __future__ import absolute_import, division, print_function


def resolve(env, version):
    return [
        ('javac', None),
        ('javah', None),
        ('jar', None),
    ]

# vim: set et ts=4 sw=4 ai :
